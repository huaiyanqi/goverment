package com.trs.jdbc;

import com.mysql.jdbc.Connection;
import com.trs.govutil.ConstantUtil;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCIIP{
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_url= ConstantUtil.jdbc_urlIIP;
    
    public static final String  jdbc_username=  ConstantUtil.jdbc_usernameIIP;
    public static final String  jdbc_password= ConstantUtil.jdbc_passwordIIP;
    static final String DB_URL = jdbc_url;
    // MySQL的JDBC URL编写方式：jdbc:mysql://主机名称：连接端口/数据库的名称
    static final String USER = jdbc_username;
    static final String PASS = jdbc_password;

    public  Integer   JDBCDriver(String sql) throws SQLException,Exception{
       java.sql.Connection conn = null;
       Statement stat = null;
       Integer rs=null;
       try {
       // 注册驱动
       Class.forName(jdbc_driver);

       // 创建链接
       conn = DriverManager.getConnection(DB_URL,USER,PASS);

       // 执行sql
       stat = conn.createStatement();  
       rs = stat.executeUpdate(sql);

       } catch (SQLException e) {
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } catch (Exception e) {
           e.printStackTrace();
       } finally {
           conn.close();
       }
	return rs;

   }
   
    public  String    JDBCDriverSelect(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String DOCID=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
        	DOCID=String.valueOf(rs.getInt("DOCID"));
//            System.out.print("\n");
            
        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return DOCID;


    }
    
    /**
     * 根据id查询栏目id
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    
    public  String    JDBCDriverSelectChID(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String CHNLID=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
        	CHNLID=String.valueOf(rs.getInt("CHNLID"));
//            System.out.print("\n");
            
        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return CHNLID;


    }
    
    /**
     * 查询发布地址
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public static  String    JDBCDriverSelectPubUrl(String docid) throws SQLException,Exception{
    	
    	String sql="SELECT * FROM wcmdocument WHERE DOCID = '"+docid+"'";
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String DOCPUBURL=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
        	DOCPUBURL=String.valueOf(rs.getString("DOCPUBURL"));
//            System.out.print("\n");
            
        }
        System.out.println(DOCPUBURL);
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return DOCPUBURL;


    }
    
    
    /**
     * 根据用户名称查询用户是否存在
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    
    public static  String    JDBCDriverSelectUser(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String USERID=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
        	USERID=String.valueOf(rs.getInt("USERID"));
//            System.out.print("\n");
            
        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return USERID;


    }
    
    public static void main(String[] args) {
		JDBCIIP jd= new JDBCIIP();
		try {
//			jd.JDBCDriverSelectPubUrl("UPDATE idsgroup SET KPID = 123 ,KPPARENTID =321 ,KPPARENTNAME =总经办  WHERE DISPLAYNAME = 总经办");
			String uerid=jd.JDBCDriverSelectUser("SELECT * FROM wcmuser WHERE USERNAME = 'aaaaa_zhfw'");
			System.out.println(uerid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
	}
}
