package com.trs.jdbc;

import com.mysql.jdbc.Connection;
import com.trs.govutil.ConstantUtil;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCupdatedocZIDUAN {
	
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_url= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_username=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_password= ConstantUtil.jdbc_passwordids;
    static final String DB_URL = jdbc_url;
    // MySQL的JDBC URL编写方式：jdbc:mysql://主机名称：连接端口/数据库的名称
    static final String USER = jdbc_username;
    static final String PASS = jdbc_password;
    
	static JDBCIDS  jdbcids=new JDBCIDS();
	
//	Integer  uppsd=jdbc.JDBCDriver("update kpuser set password='"+psssword+"' where username='"+USERNAME+"'");	
	
//	String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
//	String viewName=jdbcids.JDBCVIEWIDSELECT(sql);   webhdjlzwxx
	
	public static void main(String[] args) throws SQLException, Exception {
		
		//站点id
		String  siteid="170";
		String jijie="第15届";
		String chid="49897";
		String jieshu="15";
		updocZiduan(jijie,siteid,chid,jieshu);
	}

	public static void  updocZiduan(String jijie,String siteid,String chid,String jieshu) throws Exception{
		
		String selectsql="SELECT MetaDataId from wcmmetatablewebldjl WHERE cjgzsj = '"+jijie+"' AND MetaDataId in (select DOCID FROM wcmdocument WHERE SITEID =  '"+siteid+"' AND DOCCHANNEL = '"+chid+"' AND DOCSTATUS = '10' ) ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	int id = rs.getInt("MetaDataId"); 
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			//查到id  直接进行 更改两个表 字段
			//先更新 wcmdocument
			String upsqldoc="update wcmmetatablewebldjl  set  fbt = '"+jieshu+"'  WHERE  MetaDataId= '"+id+"'";
			System.out.println(upsqldoc);
			JDBCDriver(upsqldoc);
			
			
        }
        
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
	
	}
	
    public static  void   JDBCDriver(String sql) throws SQLException,Exception{
        java.sql.Connection conn = null;
        Statement stat = null;
        
        try {
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行sql
        stat = conn.createStatement();  
        stat.executeUpdate(sql);
        //执行完毕  关闭连接
        stat.close();
        conn.close();
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    
    }
   
}
