package com.trs.jdbc;

import com.mysql.jdbc.Connection;
import com.trs.govutil.ConstantUtil;
import com.trs.govutil.HttpUtil;
import com.trs.govutil.Util;

import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JDBCWCM {
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  DB_URL= ConstantUtil.jdbc_urlwcm;

    public static final String  USER=  ConstantUtil.jdbc_usernamewcm;
    public static final String  PASS= ConstantUtil.jdbc_passwordwcm;
    public static final String  HY_DOW_URL= ConstantUtil.HY_DOW_URL;





    /**
     * 查询稿件中间表数量
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static int  sumSequenceCount() throws Exception{

        String selectsql=" SELECT IFNULL(sum(documentCount),0) as documentCount FROM xwcmgovopendocument  ";
        Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        int   documentCount=0;
        while(rs.next()){  // 遍历结果集ResultSet
            //获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
            documentCount = rs.getInt("documentCount");//附件
        }
        //6. 释放资源
        rs.close();  // 释放次序不能乱
        stat.close();
        conn.close();
        return documentCount;

    }


    /**
     * 处理稿件得附件
     * @return
     * @throws Exception
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List  selectFile(String docid,String docPubUrl) throws Exception{

        System.out.println("--------------------------------------开始处理附件--------------------------------------------------");

//		String selectsql=" SELECT * from wcmappendix where appdocid ='21' ";
        String selectsql=" SELECT ax.* from wcmappendix ax ,wcmdocument doc ,wcmchnldoc chnl where chnl.docid='"+docid+"' and ax.appdocid =doc.docid  AND doc.docid=chnl.docid  ";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);

        Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);


        List reslist=new ArrayList<>();


        while(rs.next()){  // 遍历结果集ResultSet
            HashMap res=new HashMap<>();
            //获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
            String  APPENDIXID = rs.getString("APPENDIXID");//附件
            String  APPDOCID = rs.getString("APPDOCID");//附件后缀
            String  appflag = rs.getString("APPFLAG");//附件后缀
            String  APPSERN = rs.getString("APPSERN");//0  正文中图片  130 是附件 稿件背景得附件      20  微信封面    10 是附件字段得附件
            String  APPDESC = rs.getString("APPDESC");//TopPic=1  首页封面图
            String  APPFILE = rs.getString("APPFILE");
            if("10".equals(appflag)){
                res.put("fileno",String.valueOf(APPSERN));
                res.put("documentTitle",APPDESC);
                res.put("fileName",APPFILE);
                if(docPubUrl==null||"".equals(docPubUrl)){
                    String url=HY_DOW_URL;
                    if(APPFILE.contains("P0")){
                        url=url+"protect/"+APPFILE.substring(0,8)+"/"+APPFILE.substring(0,10)+"/"+APPFILE;
                    }else {
                        url=url+"webpic/"+APPFILE.substring(0,8)+"/"+APPFILE.substring(0,10)+"/"+APPFILE;
                    }
                    res.put("fileurl",url);

                    //附件转64流,后面加密太慢非必须项注释掉
//                    try {
//                        System.out.println("转换成流的url="+url);
//                        InputStream inputStream = HttpUtil.getInputStream(url);
//                        String base64String = HttpUtil.inputStreamToBase64(inputStream);
//                        res.put("filecontent",base64String);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }else{
                    String fileUrl = docPubUrl.substring(0, docPubUrl.lastIndexOf("/") + 1) + APPFILE;
                    res.put("fileurl",fileUrl);
                }
                reslist.add(res);
            }

        }

        System.out.println("处理附件结果：："+reslist);

        //6. 释放资源
        rs.close();  // 释放次序不能乱
        stat.close();
        conn.close();
        System.out.println("--------------------------------------处理附件结束--------------------------------------------------");

        return reslist;

    }


    /**
     * 查询数据  咨询视图数据
     * @param docid
     * @return
     * @throws Exception
     */
    public static HashMap<String, String> selectZXDoc(String docid) throws Exception{

        System.out.println("--------------------------------------政策文件试图规章和行为规范性文件-处理基础字段开始--------------------------------------------------");
        String selectsql="  SELECT zx.*, doc.*,chnl.* from wcmmetatablewebzcwj zx ,wcmdocument doc ,wcmchnldoc chnl where chnl.docid='"+docid+"' and zx.MetaDataId =doc.docid  AND doc.docid=chnl.docid ";
//        String selectsql=" SELECT * FROM wcmmetatablewebzcwj WHERE MetaDataId = '"+docid+"' ";
        System.out.println("查询政策文件视图数据sql："+selectsql);
        Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);

        HashMap<String , String> res=new HashMap<>();
        while(rs.next()){  // 遍历结果集ResultSet

            //获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
            String  bt = rs.getString("bt");//标题
            String  zxsj = rs.getString("zxsj");//撰写时间
            String  cwrq = rs.getString("cwrq");//成文日期
            String  jgdz = rs.getString("jgdz");//机关代字
            String  fwnf = rs.getString("fwnf");//发文年份
            String  fwxh = rs.getString("fwxh");//发文序号
//            String  gjc = rs.getString("gjc");//关键词
            String  gwzl = rs.getString("gwzl");//公文种类
            String  fwjg = rs.getString("fwjg");//发文机关
            String  ztfl = rs.getString("ztfl");//主题分类
//            String  zy = rs.getString("zy");//摘要
            String  ly = rs.getString("ly");//来源
            String  ssrq = rs.getString("ssrq");//实施日期
            String  fzrq = rs.getString("fzrq");//废止日期
            String  wjyxx = rs.getString("wjyxx");//文件有效性
            String  wjsssj = rs.getString("wjsssj");//文件所属司局
            String  tz = rs.getString("fbt");//副标题
            String  docPubUrl =rs.getString("docPubUrl");//发布地址
            String  zw =rs.getString("zw");//html正文
            String  siteid =rs.getString("SITEID");//html正文




            res.put("bt", bt);
            res.put("zxsj", zxsj);
            res.put("cwrq", cwrq);
            res.put("jgdz", jgdz);
            res.put("fwnf", fwnf);
            res.put("fwxh", fwxh);
//            res.put("gjc",gjc);
            res.put("gwzl", gwzl);
            res.put("fwjg",fwjg);
            res.put("ztfl", ztfl);
//            res.put("zy", zy);
            res.put("ly", ly);
            res.put("ssrq", ssrq);
            res.put("fzrq", fzrq);
            res.put("wjyxx", wjyxx);
            res.put("tz", tz);
            res.put("wjsssj", wjsssj);
            res.put("zw", zw);
            res.put("docPubUrl", docPubUrl);
            res.put("siteid", siteid);

            System.out.println(res);
//        	System.out.println("-----------------------------一组分割线--------------------------------");

        }
        System.out.println("政策文件视图基础字段："+res);

        //6. 释放资源
        rs.close();  // 释放次序不能乱
        stat.close();
        conn.close();
        System.out.println("--------------------------------------政策文件试图规章和行为规范性文件-处理基础字段结束--------------------------------------------------");
        return res;

    }


    /**
     * 查询xwcmgovopendocument docid
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public  String  selectGovOpenDoc(String sql) throws SQLException,Exception{
        Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
        System.out.println("查询sql："+sql);
        String  docId=null;
        // 输出查询结果
        while(rs.next()){
            System.out.print(rs.getInt("id")+",");
            System.out.print("docId:"+rs.getString("docId")+",");
            docId=rs.getString("docId");
        }
        // 关闭
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            Util.log("MySQL操作错误:"+e,"log",0);
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } finally {
            try {
                if (stat != null) {
                    stat.close();
                }
            } catch (SQLException e) {
                Util.log("MySQL操作错误:"+e,"log",0);
                System.out.println("MySQL操作错误");
                e.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return docId;


    }



    /**
     * 查询海云站点名称
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public static  String  JDBCSelectSitename(String sql) throws SQLException,Exception{
        Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);


        System.out.println("查询sql："+sql);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
        String sitename="无";
        while(rs.next()){
            sitename=rs.getString("SITENAME");
        }
        // 关闭
        rs.close();
        return sitename;

    }

    /**
     * 执行inster  update语句
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public  static Integer   JDBCDriver(String sql) throws SQLException,Exception{
        java.sql.Connection conn = null;
        Statement stat = null;
        Integer rs=null;
        try {
            // 注册驱动
            Class.forName(jdbc_driver);

            // 创建链接
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            // 执行sql
            stat = conn.createStatement();
            rs = stat.executeUpdate(sql);

        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
        return rs;

    }

    public  String    JDBCDriverSelect(String sql) throws SQLException,Exception{
        Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String DOCID=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
            DOCID=String.valueOf(rs.getInt("DOCID"));
//            System.out.print("\n");

        }
        // 关闭
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stat != null) {
                    stat.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return DOCID;


    }

    /**
     * 根据id查询栏目id
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */

    public  String    JDBCDriverSelectChID(String sql) throws SQLException,Exception{
        Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String CHNLID=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
            CHNLID=String.valueOf(rs.getInt("CHNLID"));
//            System.out.print("\n");

        }
        // 关闭
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stat != null) {
                    stat.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return CHNLID;


    }

    /**
     * 查询发布地址
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public static  String    JDBCDriverSelectPubUrl(String docid) throws SQLException,Exception{

        String sql="SELECT * FROM wcmdocument WHERE DOCID = '"+docid+"'";
        Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String DOCPUBURL=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
            DOCPUBURL=String.valueOf(rs.getString("DOCPUBURL"));
//            System.out.print("\n");

        }
        System.out.println(DOCPUBURL);
        // 关闭
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stat != null) {
                    stat.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return DOCPUBURL;


    }


    /**
     * 根据用户名称查询用户是否存在
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */

    public static  String    JDBCDriverSelectUser(String sql) throws SQLException,Exception{
        Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
        // 输出查询结果
        String USERID=null;
        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
            USERID=String.valueOf(rs.getInt("USERID"));
//            System.out.print("\n");

        }
        // 关闭
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stat != null) {
                    stat.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return USERID;


    }

    public static void main(String[] args) {
        JDBCWCM jd= new JDBCWCM();
        try {
//			jd.JDBCDriverSelectPubUrl("UPDATE idsgroup SET KPID = 123 ,KPPARENTID =321 ,KPPARENTNAME =总经办  WHERE DISPLAYNAME = 总经办");
            String uerid=jd.JDBCDriverSelectUser("SELECT * FROM wcmuser WHERE USERNAME = 'aaaaa_zhfw'");
            System.out.println(uerid);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
