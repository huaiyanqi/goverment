package com.trs.jdbc;

import com.mysql.jdbc.Connection;
import com.trs.govutil.ConstantUtil;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCupdatedoclink {
	
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_url= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_username=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_password= ConstantUtil.jdbc_passwordids;
    static final String DB_URL = jdbc_url;
    // MySQL的JDBC URL编写方式：jdbc:mysql://主机名称：连接端口/数据库的名称
    static final String USER = jdbc_username;
    static final String PASS = jdbc_password;
    
	static JDBCIDS  jdbcids=new JDBCIDS();
	
//	Integer  uppsd=jdbc.JDBCDriver("update kpuser set password='"+psssword+"' where username='"+USERNAME+"'");	
	
//	String sql="SELECT * FROM xwcmviewinfo WHERE VIEWINFOID='"+viewid+"'";
//	String viewName=jdbcids.JDBCVIEWIDSELECT(sql);   webhdjlzwxx
	
	public static void main(String[] args) throws SQLException, Exception {
		
		// 第几次循环
		int count=0;
		//站点id
		String siteid="170";
		
		//需要变更得 数据 总数量
		int con = 23861 ;
		con=con/1000+1;
		for(int i=0;i<con;i++) {
			updoclink(siteid,count);
			count++;
		}
	}
	
	public static void  updoclink(String siteid,int count) throws Exception{
		
		int start=count*1000;
		
		String selectsql="SELECT docid FROM wcmdocument WHERE siteid = '"+siteid+"'  AND  doctype = 20  LIMIT "+start+",1000";
//		String selectsql="select * from  "+tablename+" where MetaDataId = '1682075' ";
        System.out.println("查询sql："+selectsql);
        
    	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);
        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(selectsql);
        
        while(rs.next()){  // 遍历结果集ResultSet
        	
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
//        	System.out.println("-----------------------------一组分割线--------------------------------");
        	int id = rs.getInt("docid"); 
//        	System.out.println("-----------------------------一组分割线--------------------------------");
			//查到id  直接进行 更改两个表 字段
			//先更新 wcmdocument
//			String upsqldoc="update wcmdocument doc set doc.DOCLINK = null  WHERE doc.siteid = '"+siteid+"'  AND  doc.doctype = 20  AND docid = "+id+"";
//			JDBCDriver(upsqldoc);
			
			//在更新  wcmmetatablegovdocnews
			String upsqlgov="update wcmmetatablegovdocnews gov set gov.DocLink = null  WHERE  gov.DocType = 20  AND gov.MetaDataId = '"+id+"' ";
			System.out.println("更新wcmmetatablegovdocnews表sql"+upsqlgov);
			JDBCDriver(upsqlgov);
			
        }
        
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
        
	
	}
	
    public static  void   JDBCDriver(String sql) throws SQLException,Exception{
        java.sql.Connection conn = null;
        Statement stat = null;
        
        try {
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行sql
        stat = conn.createStatement();  
        stat.executeUpdate(sql);
        //执行完毕  关闭连接
        stat.close();
        conn.close();
        } catch (SQLException e) {
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.close();
        }
    
    }
   
}
