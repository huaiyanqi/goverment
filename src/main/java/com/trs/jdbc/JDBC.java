package com.trs.jdbc;

import com.mysql.jdbc.Connection;
import com.trs.govutil.ConstantUtil;
import com.trs.govutil.Util;


import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC {
	
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_url= ConstantUtil.jdbc_url;
    
    public static final String  jdbc_username=  ConstantUtil.jdbc_username;
    public static final String  jdbc_password= ConstantUtil.jdbc_password;
    static final String DB_URL = jdbc_url;
    // MySQL的JDBC URL编写方式：jdbc:mysql://主机名称：连接端口/数据库的名称
    static final String USER = jdbc_username;
    static final String PASS = jdbc_password;
    
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;


	
	/**
	 * 插入数据
	 * @param sql
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
    public  Integer   JDBCDriver(String sql) throws SQLException,Exception{
       java.sql.Connection conn = null;
       Statement stat = null;
       Integer rs=null;
       try {
       // 注册驱动
       Class.forName(jdbc_driver);

       // 创建链接
       conn = DriverManager.getConnection(DB_URL,USER,PASS);

       // 执行sql
       stat = conn.createStatement();  
       rs = stat.executeUpdate(sql);

       } catch (SQLException e) {
    	   Util.log("MySQL操作错误:"+e,"log",0);
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } catch (Exception e) {
           e.printStackTrace();
       } finally {
           conn.close();
       }
	return rs;

   }
    
    
    
   /**
    * 查询海云的sql
    * @param sql
    * @return
    * @throws SQLException
    * @throws Exception
    */
    public  String   JDBCDriverSelect(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
        System.out.println("查询sql："+sql);
        String  haiyun_id=null;
        // 输出查询结果
        while(rs.next()){
            System.out.print(rs.getInt("id")+",");
            System.out.print("haiyun_id:"+rs.getString(FIELDHY)+",");
            haiyun_id=rs.getString(FIELDHY);
        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
    	   Util.log("MySQL操作错误:"+e,"log",0);
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
        	   Util.log("MySQL操作错误:"+e,"log",0);
               System.out.println("MySQL操作错误");
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return haiyun_id;


    }
    
    /**
     * 查询开普 id的
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public  String   JDBCDriverSelectKP(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
        System.out.println("查询sql："+sql);
        String  kaipu_id=null;
        // 输出查询结果
        while(rs.next()){
            System.out.print(rs.getInt("id")+",");
            System.out.print("kaipu_id:"+rs.getString(FIELDKP)+",");
            kaipu_id=rs.getString(FIELDKP);
        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
    	   Util.log("MySQL操作错误:"+e,"log",0);
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
        	   Util.log("MySQL操作错误:"+e,"log",0);
               System.out.println("MySQL操作错误");
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return kaipu_id;


    }
    
    /**
     * 查询开普 id的
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public static  String   SelectKpuserid(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
        System.out.println("查询sql："+sql);
        String  kpuserid=null;
        // 输出查询结果
        while(rs.next()){
            System.out.print("kpuserid:"+rs.getString("userid")+",");
            kpuserid=rs.getString("userid");
        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
    	   Util.log("MySQL操作错误:"+e,"log",0);
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
        	   Util.log("MySQL操作错误:"+e,"log",0);
               System.out.println("MySQL操作错误");
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return kpuserid;


    }
    
    /**
     * 查询海云的父id的
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public  String   JDBCDriverSelectPARENTID(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
        System.out.println("查询sql："+sql);
        String  parentid_hy=null;
        // 输出查询结果
        while(rs.next()){
            System.out.print(rs.getInt("id")+",");
            System.out.print("PARENTID:"+rs.getString(PARENTID)+",");
            parentid_hy=rs.getString(PARENTID);
        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
    	   Util.log("MySQL操作错误:"+e,"log",0);
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
        	   Util.log("MySQL操作错误:"+e,"log",0);
               System.out.println("MySQL操作错误");
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return parentid_hy;


    }
    
    
    /**
     * 查询开普同步用户的密码
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
     public  String   JDBCDriverSelectUserPassword(String sql) throws SQLException,Exception{
     	Connection conn = null;
         Statement stat = null;

         // 注册驱动
         Class.forName(jdbc_driver);

         // 创建链接
         conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

         // 执行查询
         stat = conn.createStatement();
         ResultSet rs = stat.executeQuery(sql);
//         rs.last();
         System.out.println("查询sql："+sql);
         String  passwordKp=null;
         // 输出查询结果
         while(rs.next()){
             System.out.print(rs.getInt("id")+",");
             System.out.print("password:"+rs.getString("password")+",");
             passwordKp=rs.getString("password");
         }
         // 关闭
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
     	   Util.log("MySQL操作错误:"+e,"log",0);
            System.out.println("MySQL操作错误");
            e.printStackTrace();
        } finally {
            try {
                if (stat != null) {
                    stat.close();
                }
            } catch (SQLException e) {
         	   Util.log("MySQL操作错误:"+e,"log",0);
                System.out.println("MySQL操作错误");
                e.printStackTrace();
            } finally {
                try {
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
 	return passwordKp;


     }
    
     
     /**
      * 查询sql
      * @param sql
      * @return
      * @throws SQLException
      * @throws Exception
      */
      public static  ResultSet   JDBCDriverSelectSql(String sql) throws SQLException,Exception{
      	Connection conn = null;
          Statement stat = null;

          // 注册驱动
          Class.forName(jdbc_driver);

          // 创建链接
          conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

          // 执行查询
          stat = conn.createStatement();
          ResultSet rs = stat.executeQuery(sql);
//          rs.last();
          System.out.println("查询sql："+sql);
          // 关闭
         try {
             if (rs != null) {
                 rs.close();
             }
         } catch (SQLException e) {
      	   Util.log("MySQL操作错误:"+e,"log",0);
             System.out.println("MySQL操作错误");
             e.printStackTrace();
         } finally {
             try {
                 if (stat != null) {
                     stat.close();
                 }
             } catch (SQLException e) {
          	   Util.log("MySQL操作错误:"+e,"log",0);
                 System.out.println("MySQL操作错误");
                 e.printStackTrace();
             } finally {
                 try {
                     if (conn != null) {
                         conn.close();
                     }
                 } catch (SQLException e) {
                     e.printStackTrace();
                 }
             }
         }
         return rs;


      }
    
      
      
      /**
       * 查询sql
       * @param sql
       * @return
       * @throws SQLException
       * @throws Exception
       */
       public static String  selectYsjjid(String sql) throws SQLException,Exception{
       	Connection conn = null;
           Statement stat = null;

           // 注册驱动
           Class.forName(jdbc_driver);

           // 创建链接
           conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

           // 执行查询
           stat = conn.createStatement();
           ResultSet rs = stat.executeQuery(sql);
//           rs.last();
           System.out.println("查询sql："+sql);
           String ysjjid="";
           while(rs.next()){
               System.out.print("ysjjid:"+rs.getString("ysjjid"));
               ysjjid=rs.getString("ysjjid");
           }
           // 关闭
          try {
              if (rs != null) {
                  rs.close();
              }
          } catch (SQLException e) {
       	   Util.log("MySQL操作错误:"+e,"log",0);
              System.out.println("MySQL操作错误");
              e.printStackTrace();
          } finally {
              try {
                  if (stat != null) {
                      stat.close();
                  }
              } catch (SQLException e) {
           	   Util.log("MySQL操作错误:"+e,"log",0);
                  System.out.println("MySQL操作错误");
                  e.printStackTrace();
              } finally {
                  try {
                      if (conn != null) {
                          conn.close();
                      }
                  } catch (SQLException e) {
                      e.printStackTrace();
                  }
              }
          }
          return ysjjid;


       }
    public static void main(String[] args) {
    	JDBC  aaa=new JDBC();
    	try {
    		
//    		int kaipu_id =aaa.JDBCDriver("DELETE  FROM  kaipuhaiyun  where source='BM' and haiyun_id =39");

			
			String kaipu_id =aaa.JDBCDriverSelectKP("SELECT * FROM "+TABLE+" where "+SOURCE+"='ZD' and "+FIELDHY+"='1'");
			
			System.out.println("kaipu_id:"+kaipu_id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
