package com.trs.jdbc;

import com.mysql.jdbc.Connection;
import com.trs.govutil.ConstantUtil;
import com.trs.govutil.Util;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCIDS{
    public static final String  jdbc_driver=  ConstantUtil.jdbc_driver;
    public static final String  jdbc_url= ConstantUtil.jdbc_urlids;
    
    public static final String  jdbc_username=  ConstantUtil.jdbc_usernameids;
    public static final String  jdbc_password= ConstantUtil.jdbc_passwordids;
    static final String DB_URL = jdbc_url;
    // MySQL的JDBC URL编写方式：jdbc:mysql://主机名称：连接端口/数据库的名称
    static final String USER = jdbc_username;
    static final String PASS = jdbc_password;

    public  Integer   JDBCDriver(String sql) throws SQLException,Exception{
       java.sql.Connection conn = null;
       Statement stat = null;
       Integer rs=null;
       try {
       // 注册驱动
       Class.forName(jdbc_driver);

       // 创建链接
       conn = DriverManager.getConnection(DB_URL,USER,PASS);

       // 执行sql
       stat = conn.createStatement();  
       rs = stat.executeUpdate(sql);

       } catch (SQLException e) {
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } catch (Exception e) {
           e.printStackTrace();
       } finally {
           conn.close();
       }
	return rs;

   }
   
    public  ResultSet   JDBCDriverSelect(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        // 输出查询结果
//        while(rs.next()){
//            System.out.print(rs.getInt("id")+",");
//            System.out.print(rs.getString("name")+",");
//            System.out.print(rs.getString("sex")+",");
//            System.out.print(rs.getInt("age"));
//            System.out.print("\n");
//        }
        // 关闭
//       try {
//           if (rs != null) {
//               rs.close();
//           }
//       } catch (SQLException e) {
//           e.printStackTrace();
//       } finally {
//           try {
//               if (stat != null) {
//                   stat.close();
//               }
//           } catch (SQLException e) {
//               e.printStackTrace();
//           } finally {
//               try {
//                   if (conn != null) {
//                       conn.close();
//                   }
//               } catch (SQLException e) {
//                   e.printStackTrace();
//               }
//           }
//       }
	return rs;


    }
    
    public static  String   JDBCDriverSelectUserName(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        // 输出查询结果
        String truename= null;
        while(rs.next()){
        	
        	truename=rs.getString("truename");
            System.out.print("当前登录用户真是姓名："+rs.getString("truename"));

        }
        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return truename;


    }
    
    /**
     * 查询开普 id的
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public  String   JDBCVIEWIDSELECT1(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
        System.out.println("查询sql："+sql);
        String  MAINTABLENAME=null;
        // 输出查询结果
//        while(rs.next()){
////            System.out.print(rs.getInt("id")+",");
//            System.out.print("MAINTABLENAME:"+rs.getString(MAINTABLENAME)+",");
//            MAINTABLENAME=rs.getString(MAINTABLENAME);
//        }
        
        while(rs.next()){  // 遍历结果集ResultSet
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
			int id = rs.getInt("VIEWINFOID");  // 参数表示列名 (不建议使用int获取第几列)
			String name = rs.getString("VIEWNAME");
			Double price = rs.getDouble("VIEWDESC");
			System.out.println(id);
			System.out.println(name);
			System.out.println(price);
		}

        // 关闭
       try {
           if (rs != null) {
               rs.close();
           }
       } catch (SQLException e) {
    	   Util.log("MySQL操作错误:"+e,"log",0);
           System.out.println("MySQL操作错误");
           e.printStackTrace();
       } finally {
           try {
               if (stat != null) {
                   stat.close();
               }
           } catch (SQLException e) {
        	   Util.log("MySQL操作错误:"+e,"log",0);
               System.out.println("MySQL操作错误");
               e.printStackTrace();
           } finally {
               try {
                   if (conn != null) {
                       conn.close();
                   }
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }
       }
	return MAINTABLENAME;


    }
    
    
    /**
     * 查询开普 id的
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public  ResultSet   JDBCSelectAllView(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        
        System.out.println("查询sql："+sql);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();

        
        // 关闭
      
	return rs;


    }
    
    
    public  String   JDBCVIEWIDSELECT(String sql) throws SQLException,Exception{
    	
       	Connection conn = null;
        Statement stat = null;
        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);
		//3. 获取执行SQL语句对象 (负责将SQL语句发送给数据库)
		stat = conn.createStatement(); // con.prepareStatement()，可以防sql注入,也可以预编译sql语句提高效率
		//4. 执行SQL语句获取结果集
		ResultSet rs = stat.executeQuery(sql);  // 执行查询语句用executeQuery
		String  MAINTABLENAME=null;
		//5. 处理结果集
        while(rs.next()){  // 遍历结果集ResultSet
			//获取这条记录中每列数据,使用ResultSet接口的方法 getXX方法,参数建议写String列名
        	MAINTABLENAME = rs.getString("MAINTABLENAME");
		}
		
		//6. 释放资源
		rs.close();  // 释放次序不能乱
		stat.close();
		conn.close();
		
		return MAINTABLENAME;
	}

    
    /**
     * 查询海云站点名称
     * @param sql
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public static  String  JDBCSelectSitename(String sql) throws SQLException,Exception{
    	Connection conn = null;
        Statement stat = null;

        // 注册驱动
        Class.forName(jdbc_driver);

        // 创建链接
        conn = (Connection) DriverManager.getConnection(DB_URL,USER,PASS);

        
        System.out.println("查询sql："+sql);
        // 执行查询
        stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);
//        rs.last();
		String sitename="无";
		while(rs.next()){
			sitename=rs.getString("SITENAME");
		}
        // 关闭
        rs.close();
        return sitename;

    }
    
    public static void main(String[] args) {
		JDBCIDS jd= new JDBCIDS();
		try {
			jd.JDBCDriverSelect("UPDATE idsgroup SET KPID = 123 ,KPPARENTID =321 ,KPPARENTNAME =总经办  WHERE DISPLAYNAME = 总经办");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
	}
}
