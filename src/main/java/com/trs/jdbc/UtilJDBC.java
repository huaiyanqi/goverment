package com.trs.jdbc;

import com.trs.govutil.ConstantUtil;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilJDBC {
	
	private static String LOCALLOG = ConstantUtil.LOCALLOG;
	 public static SimpleDateFormat fmtAll = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
    public static String getNowTime() {
        return fmtAll.format(new Date());
    }
	
    /**
     * 防止空指针
     * @param obj
     * @return
     */
    public static String toStr(Object obj) {

        if (obj == null) {
            return "";
        } else {
            return obj.toString().trim();
        }
    }
	/**
     * LOG输出xml
     * @param in
     * @param name
     * @param type 1:打印控制台
     */
    public static void log(Object in,String name, int type){
        if(name.equals("")){
            name = "debug";
        }
        String content = UtilJDBC.toStr(in);
        if(type == 1) {
            System.out.println(content);
        }
        String path=LOCALLOG;
        path = path.replace("\\", "/");
        if(!path.endsWith("/")){
            path += "/";
        }
        File folder = new File(path);
        if(!folder.exists() && !folder.isDirectory())
        {
            folder.mkdirs();
        }
        File file=null;
        String date = UtilJDBC.getNowTime().substring(0, 10);
        file = new File(path + date + "-" + name + ".log");
        if(!file.exists())
        {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
            writer.write( "\r\n" + content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
