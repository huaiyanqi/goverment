package com.trs.govservers;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.govutil.Util;
import com.trs.hy.HttpFileUpload;
import com.trs.jdbc.JDBC;
import com.trs.jdbc.JDBCWCM;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 * @author epro1
 *
 */
public class GovServices {





	/**
	 * 根据站点id查询站点名称
	 * @param SITEID
	 * @return
	 * @throws Exception
	 */
	public static String selectSiteName(String SITEID) throws Exception{

		String sql="SELECT SITENAME FROM wcmwebsite WHERE SITEID ='"+SITEID+"'";
		String  sn=JDBCWCM.JDBCSelectSitename(sql);
		return sn;
	}


	/**
	 *查询中间表存储数据数量
	 * @return
	 * @throws Exception
	 */
	public int sumSequenceCount() throws Exception {

		JDBCWCM jdbc=new JDBCWCM();
		int sumSequenceCount = jdbc.sumSequenceCount();

		return sumSequenceCount;
	}


	/**
	 *
	 * 处理附件
	 * @param docId
	 * @param docPubUrl
	 * @return
	 * @throws Exception
	 */
	public List formateAppFile(String docId, String docPubUrl) throws Exception {

		JDBCWCM jdbc=new JDBCWCM();
		List wcmappendixEList = jdbc.selectFile(docId,docPubUrl);

		return wcmappendixEList;
	}

    /**
     * 查询 xwcmgovopendocument
     * @param
     * @return
     * @throws SQLException
     * @throws Exception
     */
    @SuppressWarnings("static-access")
	public HashMap<String, String> selectZXDoc(String ID) throws SQLException, Exception{
        JDBCWCM jdbc=new JDBCWCM();
        HashMap<String, String> docMap=jdbc.selectZXDoc(ID);

        return docMap;
    }


	/**
	 * 查询 xwcmgovopendocument
	 * @param
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public String doCheck(String ID) throws SQLException, Exception{
		JDBCWCM jdbc=new JDBCWCM();
		String sql="SELECT * FROM  xwcmgovopendocument  where  docId='"+ID+"'";;
		String  docid=null;
		docid=jdbc.selectGovOpenDoc(sql);

		return docid;
	}



	/**
	 * 文档查询id
	 * @return  1  z站点 2 栏目
	 * @throws SQLException
	 * @throws Exception
	 */
	public String docCheck(String ID,int index) throws SQLException, Exception{
		JDBC  jdbc=new JDBC();
		String sql=null;
		String  kSitId=null;
		if(index == 1){
			sql="";
		}else{
			sql="";
		}
		System.out.println("===========================新增栏目查询栏目idsql========================");
		System.out.println(sql);
		System.out.println("===========================新增栏目查询栏目idsql========================");
		kSitId=jdbc.JDBCDriverSelectKP(sql);

		return kSitId;
	}



	/**
	 * 处理正文方法
	 * @param content
	 * @param sub
	 * @param regular
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	public static String contentResource(String content,int sub,String regular,String fburl) throws IOException {

		//截取发布地址
		String pu=fburl.substring(0,fburl.lastIndexOf("/"));
		String regExImg = regular;//正文中图片
		List<String> list=getContentResource(regExImg, sub, content);
		for(int i = 0 ; i < list.size() ; i++) {
			System.out.println("正文附件路径：：：：：："+list.get(i));
			if(list.get(i).contains("protect")||list.get(i).contains("webpic")){

				//获取名称
				String name=list.get(i).substring(list.get(i).lastIndexOf("/")+1,list.get(i).length());
				//处理附件名称带特殊字符
				name=filterSpecialChar(name);
				//拼接外网访问地址
				String webUrl=pu+"/"+name;
				System.out.println("正文外网访问地址：：："+webUrl);
				//判断附件名称是不是符合海云规则 也可能存在 包含  W P 得 其他附件 概率比较低
				content=content.replace(list.get(i),webUrl);

			}else{
				System.out.println("不是protect/webpic开头附件，默认不做处理");
			}

		}
		return content;
	}
	/**
	 * 获取正文内资源
	 * List<String> list = new ArrayList<String>();
	 */
	public static List<String>  getContentResource(String regExImg,int subStart,String content){
		List<String> list = new ArrayList<String>();
		Pattern patternImg = Pattern.compile(regExImg);
		Matcher matcherImg = patternImg.matcher(content);
		while (matcherImg.find()) {
			String picPath = matcherImg.group();
			if(picPath.contains("protect")||picPath.contains("webpic")){
				picPath = picPath.substring(subStart, picPath.length()-1);
//				String fullPath = getFullPath(picPath);
				System.out.println("匹配到的路径："+picPath);
				list.add(picPath);
			}

		}
		List<String> remove=removeDuplicate(list);
		return remove;
	}


	/**
	 * 去除重复数据
	 */
	public static List<String> removeDuplicate(List<String> list) {
		HashSet<String> h = new HashSet<String>(list);
		list.clear();
		list.addAll(h);
		return list;
	}


	/**
	 * 过滤掉特殊字符
	 *
	 * @param fileName
	 * @return
	 */
	public static String filterSpecialChar(String fileName) {
		//删除所有的空格
		Pattern pattern = Pattern.compile("[\\s\\\\/:\\*\\?\\\"<>\\|]");
		Matcher matcher = pattern.matcher(fileName);
		fileName= matcher.replaceAll("_"); // 将匹配到的非法字符以空替换
		return fileName;
	}
}
