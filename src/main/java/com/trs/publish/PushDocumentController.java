package com.trs.publish;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.trs.govservers.GovServices;
import com.trs.govutil.*;
import com.trs.jdbc.JDBCWCM;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

/**
 * OAuth2.0Controller
 */
@Controller
@RequestMapping(value = "/doc")
public class PushDocumentController {
	/**
	 * 请求编码
	 */
	public static String requestEncoding = "UTF-8";
	/**
	 * 连接超时
	 */
	private static int connectTimeOut = 5000;
	/**
	 * 读取数据超时
	 */
	private static int readTimeOut = 10000;
	private static String SITE_POSTDEPT = ConstantUtil.SITE_POSTDEPT;
	private static String SJLT_URL = ConstantUtil.SJLT_URL;
	private static String Content_regular = ConstantUtil.Content_regular;
	private static String Content_regular_p = ConstantUtil.Content_regular_p;
	GovServices cR=new GovServices();
	private static String url="http://192.141.252.5/gov/file/read_file.jsp?DownName=DOCUMENT&FileName=";




	/**w
	 * 新增文档
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unused", "null", "unchecked" })
	public void pushDocument(JSONObject jsondata,String stauts,String GFWJ_flag)  throws Exception{

		System.out.println("---------------------------------------推送数据开始------------------------------------------");
		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  站点id SITEID  父栏目id  PARENTID
		String CHANNELID=jsondata.getString("CHNLID");//id
		String DOCID=jsondata.getString("DOCID");//文档id
		String SITEID=jsondata.getString("SITEID");//站点id

		boolean returnResult=false;


		String sjltid=null;
		try {
			sjltid = cR.doCheck(DOCID);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		if(sjltid != null){
			//处理数据
			Map fillPostDataDto=fillPostDataDto(DOCID,stauts,GFWJ_flag);
			System.out.println("最终组装数据：："+fillPostDataDto);
			String bt=(String )fillPostDataDto.get("documentTitle");
			String bt1=bt.toLowerCase();

			if("delete".equals(stauts)){//撤销发布

				if("seo".equals(bt1)){

					System.out.println("--------------------------------SEO文件，不做处理！！！--------");

				}else {

					//请求接口 数据加密
					String result = postDataHttp(fillPostDataDto);
					System.out.println("推送国办返回结果：："+result);
					JSONObject gbjson = JSON.parseObject(result);
					String  gbstauts = gbjson.getString("DATA");
					System.out.println("=============================================更新数据到中间表开始========================================================");
//					upDataOpenData(DOCID,fillPostDataDto,CHANNELID,-10);
					saveOpenData(DOCID,fillPostDataDto,CHANNELID,stauts);
				}

			}else if("update".equals(stauts)){//更新

				if("seo".equals(bt1)){

					System.out.println("--------------------------------SEO文件，不做处理！！！--------");

				}else {

					//请求接口 数据加密
					String result = postDataHttp(fillPostDataDto);
					System.out.println("推送国办返回结果：："+result);
					JSONObject gbjson = JSON.parseObject(result);
					String  gbstauts = gbjson.getString("DATA");
					System.out.println("=============================================更新数据到中间表开始========================================================");
//					upDataOpenData(DOCID,fillPostDataDto,CHANNELID,10);
					saveOpenData(DOCID,fillPostDataDto,CHANNELID,stauts);
				}
			}else{
				System.out.println("--------------------------------重复消息，不做处理--------");
			}

		}else{
			if("add".equals(stauts)){

				//处理数据
				Map fillPostDataDto=fillPostDataDto(DOCID,stauts,GFWJ_flag);
				System.out.println("最终组装数据：："+fillPostDataDto);
				String bt=(String )fillPostDataDto.get("documentTitle");
				String bt1=bt.toLowerCase();

				if("seo".equals(bt1)){

					System.out.println("--------------------------------SEO文件，不做处理！！！--------");

				}else {

					//请求接口 数据加密
					String result = postDataHttp(fillPostDataDto);
					System.out.println("推送国办返回结果：："+result);
					JSONObject gbjson = JSON.parseObject(result);
					String  gbstauts = gbjson.getString("DATA");

					System.out.println("=============================================新增数据到中间表开始========================================================");
					saveOpenData(DOCID,fillPostDataDto,CHANNELID,stauts);

				}


			}

		}
		System.out.println("---------------------------------------推送数据结束------------------------------------------");
	}



	public void saveOpenData(String docId, Map rootDto,String channlid,String stauts) {
		//转换成json数据存入数据库
		JSONObject json=new JSONObject(rootDto);
		//组装sql
		String sql="INSERT INTO xwcmgovopendocument (docId, docContent, documentCount, crTime, upTime, docStatus, channalID) VALUES" +
				" ( '"+docId+"', '"+json.toJSONString()+"', '1', '"+DateUtil.nowDay("yyyy-MM-dd HH-mm-ss")+"', NULL, '"+stauts+"', '"+channlid+"');";
		System.out.println("插入中间表sql："+sql);
		try {
			int re= JDBCWCM.JDBCDriver(sql);
			if(re>0){
				System.out.println("插入中间表记录成功！！！id为["+docId+"]");
			}else {
				System.out.println("插入中间表记录失败！！！id为["+docId+"]");
			}
		} catch (Exception e) {
			System.out.println("推送数据成功后，插入中间表失败！！！！id为["+docId+"]");
			e.printStackTrace();
		}
		System.out.println("=============================================新增数据到中间表结束========================================================");


	}


	public void upDataOpenData(String docId, Map rootDto,String channlid ,int stauts) {
		//转换成json数据存入数据库
		JSONObject json=new JSONObject(rootDto);
		//组装sql
		String sql=" UPDATE xwcmgovopendocument SET" +
				" docContent='"+json.toJSONString()+"', documentCount='1', crTime='"+DateUtil.nowDay("yyyy-MM-dd HH-mm-ss")+"', " +
				"upTime=NULL, docStatus='"+stauts+"', " +
				"channalID='"+channlid+"' WHERE (docId='"+docId+"'); ";
		System.out.println("更新中间表sql："+sql);
		try {
			int re= JDBCWCM.JDBCDriver(sql);
			if(re>0){
				System.out.println("更新中间表记录成功！！！id为["+docId+"]");
			}else {
				System.out.println("更新中间表记录失败！！！id为["+docId+"]");
			}
		} catch (Exception e) {
			System.out.println("推送数据成功后，更新中间表失败！！！！id为["+docId+"]");
			e.printStackTrace();
		}
		System.out.println("=============================================新增数据到中间表结束========================================================");

	}



	public  Map fillPostDataDto(String DOCID,String stauts,String GFWJ_flag) throws Exception {


		System.out.println("----------------------------处理文档开始-----------------------------");

		Map postDataDto=new HashMap();
		//jdbc查询数据
		HashMap<String, String> docMap=cR.selectZXDoc(DOCID);

		if(docMap != null && !docMap.isEmpty()){
			//处理获取结果
			String  bt = docMap.get("bt");//标题
			String  zxsj = docMap.get("zxsj");//撰写时间
			String  cwrq = docMap.get("cwrq");//成文日期
			String  jgdz = docMap.get("jgdz");//机关代字
			String  fwnf = docMap.get("fwnf");//发文年份
			String  fwxh = docMap.get("fwxh");//发文序号
//			String  gjc = docMap.get("gjc");//关键词
			String  gwzl = docMap.get("gwzl");//公文种类
			String  fwjg = docMap.get("fwjg");//发文机关
			String  ztfl = docMap.get("ztfl");//主题分类
//			String  zy = docMap.get("zy");//摘要
			String  ly = docMap.get("ly");//来源
			String  ssrq = docMap.get("ssrq");//实施日期
			String  fzrq = docMap.get("fzrq");//废止日期
			String  wjyxx = docMap.get("wjyxx");//文件有效性
			String  wjsssj = docMap.get("wjsssj");//文件所属司局
			String  tz = docMap.get("tz");//文件所属司局
			String  docPubUrl =docMap.get("docPubUrl");//发布地址
			String  zw =docMap.get("zw");//html正文
			String  siteid =docMap.get("siteid");//html正文

			if("GFXWJ".equals(GFWJ_flag)||"QTWJ".equals(GFWJ_flag)){
				//处理正文
				zw = cR.contentResource(zw, 5, Content_regular_p,docPubUrl);
				//处理 zip  pdf  rar   截取开始  href=' 6个字符串
				zw = cR.contentResource(zw, 6, Content_regular,docPubUrl);

			}else {
				//处理正文
				zw = cR.contentResource(zw, 5, Content_regular_p,docPubUrl);
				//处理 zip  pdf  rar   截取开始  href=' 6个字符串
				zw = cR.contentResource(zw, 6, Content_regular,docPubUrl);
			}



			String otherInfo1 = "规章";
//			String documentType = "命令(令)";

			//组装数据
			if ("GFXWJ".equals(GFWJ_flag)) {
				otherInfo1 = "行政规范性文件";
			}else if("QTWJ".equals(GFWJ_flag)){
				otherInfo1 = "其他文件";
			}
			postDataDto.put("otherInfo1",otherInfo1);//文件类型
			postDataDto.put("documentTitle",bt);//标题
			//文件公开发布日期
			postDataDto.put("releaseDate",formateDay(zxsj));
			//文件成文日期
			postDataDto.put("publishDate",formateDay(cwrq));
			//文件发文字号
			String wjfwzh="";
			if("令".equals(gwzl)||"命令".equals(gwzl)){
				wjfwzh=jgdz + "第" + fwxh + "号";
			}else if("公告".equals(gwzl)){
				wjfwzh=jgdz + "公告"+fwnf+"年第"+fwxh+"号";
			} else  {
				wjfwzh=jgdz + "〔" + fwnf + "〕" + "第" + fwxh + "号";
			}
			postDataDto.put("postNumber",wjfwzh);
			//发文年份
			postDataDto.put("year",fwnf);
			//发文机关代字
			postDataDto.put("agencyWord",jgdz);
			//文件顺序号
			postDataDto.put("sequenceNo",fwxh);
			//关键词
//			postDataDto.put("keyword",gjc);
			//公文种类
			postDataDto.put("documentType",gwzl);

			//查询站点名称
//			String sitename=cR.selectSiteName(siteid);
			if("".equals(fwjg)||fwjg==null){
				postDataDto.put("publisher","无");
			}else {
				//发文机关
				postDataDto.put("publisher",fwjg);
			}

			//主题分类
			postDataDto.put("themeCategory",ztfl);
			if("".equals(ly)||ly==null){
				//文件来源
				postDataDto.put("documentSource","无");
			}else {
				//文件来源
				postDataDto.put("documentSource",ly+"网站");
			}

			//文件摘要
//			postDataDto.put("abstract",zy);
			//文件正文内容
			String documentContent = "<div style='text-align: center'>" +
					"<div style='font-family:方正小标宋简体;font-size: 28px'>" + bt + "</div>" +
					"<div style='font-family:仿宋;font-size:18px'>" + tz + "</div>" +
					"</div>" +
					"<p>" + zw + "</p>";
			//行政规范性文件
			if ("GFXWJ".equals(GFWJ_flag)) {
				documentContent = "<p>" + zw + " </p>" ;
			}else if("QTWJ".equals(GFWJ_flag)){
				documentContent =  "<p>" + zw + " </p>";
			}

			postDataDto.put("documentContent",new BASE64Encoder().encode(documentContent.getBytes("UTF-8")));


			//正文附件
			postDataDto.put("appendFile",cR.formateAppFile(DOCID, docPubUrl));
			//文件解读
//			postDataDto.put("interpretation",formatInterpretation(zhengcewenjian.getZCJD()));
			/*postDataDto.setInterpretationFiles();
			postDataDto.setRelevantDocument();*/
			//文件开始实施日期
			postDataDto.put("materialData",formateDay(ssrq));
			//文件开废止日期
			postDataDto.put("abolitionDate",formateDay(fzrq));
			//有效性 1有效 0失效
			postDataDto.put("validity","1".equals(wjyxx) ? "有效" : "失效");
			//状态标识   后续处理
			if("update".equals(stauts)){
				if("0".equals(wjyxx)){
					postDataDto.put("status","abandon");//废止
				}else {
					postDataDto.put("status",stauts);
				}
			}else {
				postDataDto.put("status",stauts);
			}

			//原稿件id
			postDataDto.put("aritcleid",DOCID);
			//文件所属司局
			postDataDto.put("documentDepartment",wjsssj);
			//本次推送总数
			postDataDto.put("documentCount",1);
			//本体推送顺序号(顺序从1开始) 查询中间表记录总数
			postDataDto.put("sequenceCount",cR.sumSequenceCount() + 1);
			if("".equals(docPubUrl)||docPubUrl==null){
				//文件链接
				postDataDto.put("otherInfo2","无");
			}else {
				//文件链接
				postDataDto.put("otherInfo2",docPubUrl);
			}

			//题注
			if ("".equals(tz) || null == tz) {
				tz = "无";
			}
			postDataDto.put("otherInfo3",tz);

		}else{
			System.out.println("未查询到稿件！！稿件id["+DOCID+"]");
		}

		return  postDataDto;

	}





	public String getDateYear(String dateString) throws ParseException {
		Date date = DateUtil.formateDay(dateString, "yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int year = c.get(Calendar.YEAR);
		return String.valueOf(year);
	}

	/**
	 * 转为XXXX年XX月XX日
	 *
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public String formateDay(String dateString) throws ParseException {
		if(dateString==null||"".equals(dateString)){
			return "";
		}
		Date date = DateUtil.formateDay(dateString, "yyyy-MM-dd");
		return DateUtil.toDayString(date);
	}


	public String postDataHttp(Map rootDto) {

		//转json后加密
		JSONObject postDataJson= new JSONObject(rootDto);
		String msg = postDataJson.toJSONString();
		System.out.println("json数据：："+msg);
		String postData_sm4 = null;
		System.out.println("加密开始");
		try {

			postData_sm4 = Sm4Util.encryptEcb(SITE_POSTDEPT + DateUtil.nowDay("yyyyMMdd"), msg);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("数据加密错误！！！！！"+msg);
		}


		System.out.println("加密结束");
		Map<String, Object> map = new HashMap();
		map.put("postDept", SITE_POSTDEPT);
		map.put("postTime", String.valueOf(System.currentTimeMillis()));
		//装填加密后正文
		map.put("postData", postData_sm4);
		//String result = "";
		System.out.println("===========================================开始推送国办============================================");
		String result = HttpUtil.doPost(SJLT_URL, null, map);
//		String result = "调用国办接口";
		System.out.println("===========================================推送国办结束============================================");
		return result;
	}


	public static String doPost(String reqUrl, Map parameters,JSONObject json) {
//	    	System.out.println("------reqUrl开始------"+reqUrl);
//	    	System.out.println("------parameters开始------"+parameters);
//	    	System.out.println("------dopost开始------");
		HttpURLConnection url_con = null;
		String responseContent = null;
		try {
//	        	System.out.println("进入try：");
			String params = getMapParamsToStr(parameters, requestEncoding);
//	            System.out.println("dopost里的params"+params);
			URL url = new URL(reqUrl);
//	            System.out.println("dopost里的url"+url);
			url_con = (HttpURLConnection) url.openConnection();
			url_con.setRequestMethod("POST");
			System.setProperty("sun.net.client.defaultConnectTimeout", String.valueOf(connectTimeOut));// （单位：毫秒）jdk1.4换成这个,连接超时
			System.setProperty("sun.net.client.defaultReadTimeout", String.valueOf(readTimeOut)); // （单位：毫秒）jdk1.4换成这个,读操作超时
			url_con.setRequestProperty("User-agent","Mozilla/4.0");
			url_con.setDoOutput(true);
			byte[] b = params.toString().getBytes();
			url_con.getOutputStream().write(b, 0, b.length);
			url_con.getOutputStream().flush();
			url_con.getOutputStream().close();
			int responseCode = url_con.getResponseCode();
//	            System.out.println("responseCode:"+responseCode);
			InputStream in=null;
			if (responseCode == 200) {
				in = new BufferedInputStream(url_con.getInputStream());
			} else {
				in = new BufferedInputStream(url_con.getErrorStream());
			}
			BufferedReader rd = new BufferedReader(new InputStreamReader(in, requestEncoding));
			String tempLine = rd.readLine();
			StringBuffer tempStr = new StringBuffer();
			String crlf = System.getProperty("line.separator");
			while (tempLine != null) {
				tempStr.append(tempLine);
				tempStr.append(crlf);
				tempLine = rd.readLine();
			}
			responseContent = tempStr.toString();
			rd.close();
			in.close();
		} catch (IOException e) {
			Util.log(json,"网络故障WD",0);
			System.out.println("网络故障");
			e.printStackTrace();
		} finally {
			System.out.println("进入finally");
			if (url_con != null) {
				url_con.disconnect();
			}
		}
		return responseContent;
	}

	public static void test(){
		System.out.println("调用测试方法调用");
	}

	private static String getMapParamsToStr(Map paramMap, String requestEncoding) throws IOException {
		StringBuffer params = new StringBuffer();
		// 设置边界
		for (Iterator iter = paramMap.entrySet().iterator(); iter.hasNext(); ) {
			Map.Entry element = (Map.Entry) iter.next();
			params.append(element.getKey().toString());
			params.append("=");
			params.append(URLEncoder.encode(element.getValue().toString(), requestEncoding));
			params.append("&");
		}

		if (params.length() > 0) {
			params = params.deleteCharAt(params.length() - 1);
		}

		return params.toString();
	}

	public static void main(String[] args) throws Exception {
		PushDocumentController a=new PushDocumentController();

//		Map fillPostDataDto =a.fillPostDataDto("65160","add","GZ");

//		a.formateDay("2022-04-19 10:18:24");

//		Date date = DateUtil.formateDay("2022-04-19", "yyyy-MM-dd");
//		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//		String time = simpleDateFormat.format(date);
//		System.out.println(time);
//		StringBuffer dt=new StringBuffer(time);
//		dt.replace(4,5,"年");
//		dt.replace(7,8,"月");
//		System.out.println(dt+"日");
//		System.out.println(a.formateDay("2022-04-19 10:18:24"));
//
//		String str="https://www.mva.gov.cn/gongkai/zfxxgkpt/zc/gfxwj/202112/W020191101673972960755.jpg";
//		String cc=str.substring(0,str.lastIndexOf("/"));
		String bt="烈士公祭办法";
		String tz="(2014年3月31日民政部令第52号公布　自2014年3月31日起施行)";
		String zw="第一条　为了缅怀纪念烈士，弘扬烈士精神，做好烈士公祭工作，根据《烈士褒扬条例》，制定本办法。\n" +
				"\n" +
				"　　第二条　烈士公祭是国家缅怀纪念为民族独立、人民解放和国家富强、人民幸福英勇牺牲烈士的活动。\n" +
				"\n" +
				"　　第三条　在清明节、国庆节或者重要纪念日期间，应当举行烈士公祭活动。\n" +
				"\n" +
				"　　烈士公祭活动应当庄严、肃穆、隆重、节俭。";
		String documentContent = "<div style='text-align: center'><div style='font-family:方正小标宋简体;font-size: 28px'>" + bt + "</div>" +
				"<div style='font-family:仿宋;font-size:18px'>" + tz + "</div></div>" + "<p>" + zw + "</p>";

		System.out.println(documentContent);
	}

}