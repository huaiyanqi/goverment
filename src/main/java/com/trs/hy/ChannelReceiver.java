package com.trs.hy;

import com.trs.govutil.ConstantUtil;
import com.trs.jdbc.JDBC;

import java.sql.SQLException;


/**
 * 栏目调用方法
 * @author epro1
 *
 */
public class ChannelReceiver {
	
	private static String USER = ConstantUtil.USER;
	private static String TABLE = ConstantUtil.TABLE;
	private static String FIELDKP = ConstantUtil.FIELDKP;
	private static String FIELDHY = ConstantUtil.FIELDHY;
	private static String SOURCE = ConstantUtil.SOURCE;
	private static String PARENTID = ConstantUtil.PARENTID;
	
	/**
	 * 查询站点开普的id
	 * @param SITEID
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public String channelReceiverCheck(String SITEID) throws SQLException, Exception{
		JDBC  jdbc=new JDBC();
		String  kSitId=jdbc.JDBCDriverSelectKP("SELECT * FROM "+TABLE+" where "+SOURCE+"='ZD' and "+FIELDHY+"='"+SITEID+"'");
		
		return kSitId;
	}
	
	/**
	 * 查询栏目的开普id
	 * @param SITEID
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public String LMReceiverCheck(String SITEID) throws SQLException, Exception{
		JDBC  jdbc=new JDBC();
		String  kSitId=jdbc.JDBCDriverSelectKP("SELECT * FROM "+TABLE+" where "+SOURCE+"='LM' and "+FIELDHY+"='"+SITEID+"'");
		
		return kSitId;
	}
	
	/**
	 * 文档查询id
	 * @param SITEID
	 * @return  1  z站点 2 栏目
	 * @throws SQLException
	 * @throws Exception
	 */
	public String docCheck(String ID,int index) throws SQLException, Exception{
		JDBC  jdbc=new JDBC();
		String sql=null;
		String  kSitId=null;
		if(index == 1){
			sql="SELECT * FROM "+TABLE+" where "+SOURCE+"='ZD' and "+FIELDHY+"='"+ID+"'";
		}else{
			sql="SELECT * FROM "+TABLE+" where "+SOURCE+"='LM' and "+FIELDHY+"='"+ID+"'";
		}
		System.out.println("===========================新增栏目查询栏目idsql========================");
		System.out.println(sql);
		System.out.println("===========================新增栏目查询栏目idsql========================");
		kSitId=jdbc.JDBCDriverSelectKP(sql);
		
		return kSitId;
	}
	
	
	
	/**
	 * 文档查询id
	 * @param SITEID
	 * @return  1  z站点 2 栏目
	 * @throws SQLException
	 * @throws Exception
	 */
	public String doCheck(String ID) throws SQLException, Exception{
		JDBC  jdbc=new JDBC();
		String sql="SELECT * FROM  document  where "+SOURCE+"='WD' and "+FIELDHY+"='"+ID+"'";;
		String  kSitId=null;
		kSitId=jdbc.JDBCDriverSelectKP(sql);
		
		return kSitId;
	}
	
	
	/**
	 * 栏目查询栏目id
	 * @param SITEID
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public String channelIDCheck(String SITEID) throws SQLException, Exception{
		JDBC  jdbc=new JDBC();
		String  kSitId=jdbc.JDBCDriverSelectKP("SELECT * FROM "+TABLE+" where "+SOURCE+"='LM' and "+FIELDHY+"='"+SITEID+"'");
		
		return kSitId;
	}
	
	/**
	 * 查询站点开普的id
	 * @param
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public String docCheck(String docid,String a) throws SQLException, Exception{
		JDBC  jdbc=new JDBC();
		String  kSitId=jdbc.JDBCDriverSelectKP("SELECT * FROM  document  where "+SOURCE+"='KP' and "+FIELDHY+"='"+docid+"'");
		
		return kSitId;
	}
}
