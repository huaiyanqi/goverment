package com.trs.hy;

import allmq.Receiver;


public interface MQReceiver extends Receiver{
	void initReceiver() throws Exception;
}
