package com.trs.hy;

import java.util.ResourceBundle;


import java.util.ResourceBundle;

public class ChannelidUtil {

	static ResourceBundle resource = ResourceBundle.getBundle("channelid");

	// 从配置文件中获取IDS的Url
	
	public static final String zcwj = resource.getString("zcwj");
	public static final String zcjd = resource.getString("zcjd");
	public static final String zx = resource.getString("zx");
	public static final String lsgb = resource.getString("lsgb");
	public static final String czxx = resource.getString("czxx");
	public static final String qxzm = resource.getString("qxzm");
	
	//以下三个视图暂时不做同步
//	public static final String bmwd = resource.getString("bmwd");
//	public static final String zwmc = resource.getString("zwmc");
//	public static final String zwzsdbp = resource.getString("zwzsdbp");
	
}

