package com.trs.hy;

import com.trs.govdocument.HycloudResource;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebContextListener implements ServletContextListener{

    @Override 
    public void contextInitialized(ServletContextEvent arg0) { 
        // TODO Auto-generated method stub 
        //这里可以放你要执行的代码或方法
    	//开启站点监听
    	try {


		System.out.println("===================================MQ文章消费者开启开始=======================================");
    	//开启文档监听
    	HycloudResource hyr=new HycloudResource();
    	hyr.initMQClient();
    	hyr.initReceiver();
		System.out.println("====================================MQ文章消费者开启结束======================================");
    	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    } 
       
    @Override 
    public void contextDestroyed(ServletContextEvent arg0) { 
        // TODO Auto-generated method stub 
           
    } 
}
