package com.trs.hy;

import allmq.MQFactory;
import allmq.rabbitmq.config.ServerConfig;
import com.rabbitmq.client.Address;
import com.trs.govutil.ConstantUtil;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * mq消息队列初始化连接服务器
 * @author epro1
 *
 */
public class StartupServlet extends HttpServlet {
	
	private static String  host= ConstantUtil.MQ_ADDRESSES;
	private static String  port=ConstantUtil.MQ_PORT;
//	private static Address[] addresses= ConstantUtil.MQ_ADDRESSES;
	private static String password = ConstantUtil.MQ_PASSWORD;
	private static String virtualHost = ConstantUtil.MQ_VIRTUALHOST;
	private static String username = ConstantUtil.MQ_USERNAME;

	
	private static final long serialVersionUID = 1L;


	@Override
    protected void service(HttpServletRequest arg0, HttpServletResponse res) {
       res.setStatus(404);
    }
  
  
    @Override
    public void init(ServletConfig _servletConfig) throws ServletException {
        super.init(_servletConfig);
        initMQClient();
    }
  
    /**
    * 初始化连接的代码
    */
    private static void initMQClient() {
    	try {
			String[] hosts;
			String[] ports;
			if (host.contains(";")){
				hosts = host.split(";");
				ports = port.split(";");
			} else {
				hosts = host.split(",");
				ports = port.split(",");
			}
			if (hosts.length != ports.length && ports.length != 1) {
				
			}
			Address[] addresses = new Address[hosts.length];
			for (int i = 0; i < hosts.length; i++) {
				Address address;
				if (ports.length == 1){
					address = new Address(hosts[i], Integer.parseInt(ports[0]));
				} else {
					address = new Address(hosts[i], Integer.parseInt(ports[i]));
				}
				addresses[i] = address;
			}
			ServerConfig serverConfig = new ServerConfig(addresses, username, password, virtualHost);
			MQFactory.initConnection(serverConfig);
			System.out.println("MQ栏目消费监听启动!");
		} catch (Exception e) {
			  //这个地方记录一下日志，连接MQ服务器失败！   
			System.out.println("连接MQ服务器失败"+e);
		}  
    }
  
     
    @Override
    public void destroy() {
        // 关闭连接，不可缺少
        try {
			MQFactory.shutdown();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        super.destroy();
    }
  
}
