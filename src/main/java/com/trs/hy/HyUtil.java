package com.trs.hy;


import com.trs.hycloud.Dispatch;
import com.trs.hycloud.WCMServiceCaller;

import java.util.HashMap;
import java.util.Map;



public class HyUtil {

    /**
     * 查找海云库站点
     * @param sServiceId
     * @param sMethodName
     * @return
     * @throws Exception
     */
	public static String dataMoveDocumentHyRbj(String sServiceId,String sMethodName,Map<String, String> oPostData) throws Exception {

		Dispatch dispatch=WCMServiceCaller.Call(sServiceId, sMethodName,oPostData, true);
		String aaaa=dispatch.getResponseText();
//		System.out.println("调用海云接口查询所有站点:"+aaaa+"结束");
	    return dispatch.getResponseText();
	}
	
	
	/**
	 * 存入海云库
	 * @param args
	 */
	
	public static void main(String[] args) {
		
//		String str="22"; 
//		String s[]=str.split("[,\\.]"); 
//		for (int i = 0; i < s.length; i++) { 
//		System.out.println(i+" --->"+s[i]); 
//		}
//		
		
//----------------------------------------------------------------------
		String sServiceId = "gov_group";
		String sMethodName = "queryGroups";
		HashMap<String, String> oPostData = new HashMap<String, String>();
		oPostData.put("CurrUserName","dev"); // 当前操作的用户
//		oPostData.put("PAGESIZE","30000");
////		oPostData.put("GROUPID", "0");
//		oPostData.put("PARENTID", "34");
//		oPostData.put("PAGEINDEX", "1");
//		oPostData.put("SEARCHFIELDS","二级组织");
		

		try {
			String hy=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
			System.out.println("海云接口用户返回信息："+hy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//----------------------------------------------------------------------------------		
		//查询所有用户
//		String sServiceId = "gov_user";
//		String sMethodName = "queryUsersNoAuth";
//		Map<String, String> oPostData = new HashMap<String, String>();
//		oPostData.put("CurrUserName", "dev"); // 当前操作的用户
//		oPostData.put("pageSize", "30000");
//		String hySE=null;
//		try {
//			hySE = HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,oPostData);
//			System.out.println("海云接口用户返回信息："+hySE);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		com.alibaba.fastjson.JSONObject jsonObjectSE = JSON.parseObject(hySE);
//		Object dataArrHYSE = jsonObjectSE.get("DATA");
//		if("true".equals(jsonObjectSE.get("ISSUCCESS"))){
//			com.alibaba.fastjson.JSONObject jsonObjectHYName = JSON.parseObject(dataArrHYSE.toString());
//			com.alibaba.fastjson.JSONArray hy1=jsonObjectHYName.getJSONArray("DATA");
//			//查询海云库中要删除的用户id
//			System.out.println("================="+hy1.size());
//			if(hy1!=null){									
//				for(int a=0;a<hy1.size();a++){
//					JSONObject dataBean = (JSONObject) hy1.get(a);
//					String name=dataBean.getString("USERNAME");
//					System.out.println("用户名："+name);
//				}
//				
//				
//			}
//			
//		}
//-----------------------------------------------------------------------------------
		
	}
  
}
