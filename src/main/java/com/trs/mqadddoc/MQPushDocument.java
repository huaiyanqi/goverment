package com.trs.mqadddoc;

import com.alibaba.fastjson.JSONObject;
import com.trs.govutil.ConstantUtil;
import com.trs.govutil.Util;
import com.trs.publish.PushDocumentController;

public class MQPushDocument {

	static PushDocumentController pushDoc=new PushDocumentController();


	private static String SJLT_CHANNLID_GZ = ConstantUtil.SJLT_CHANNLID_GZ;
	private static String SJLT_CHANNLID_XZGFXWJ = ConstantUtil.SJLT_CHANNLID_XZGFXWJ;
	private static String SJLT_CHANNLID_QTWJ = ConstantUtil.SJLT_CHANNLID_QTWJ;

	/**
	 * 添加文档MQ消息
	 * @param jsondata
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static  void  pushDoc(JSONObject jsondata,String stauts) throws Exception {

		System.out.println("------------------------------=======================================MQ推送文章开始="+stauts+"===========================--------------------------------");
		//MQ消息队列数据获取

		//根据json对象中的数据名解析出相应数据    描述 CHNLDESC   状态 STATUS  站点id SITEID  父栏目id  PARENTID
		String CHANNELID=jsondata.getString("CHNLID");//id
		String DOCID=jsondata.getString("DOCID");//文档id
		String SITEID=jsondata.getString("SITEID");//站点id

		System.out.println("==========================================================================");
		System.out.println(CHANNELID+"-------------------文档的栏目id和文档id-----------------"+DOCID);
		System.out.println("==========================================================================");

		Util.log("需要推送栏目规章："+SJLT_CHANNLID_GZ,"log"+SITEID,0);
		Util.log("需要推送栏目行政规范性文件："+SJLT_CHANNLID_XZGFXWJ,"log"+SITEID,0);
		Util.log("需要推送栏目其他文件文件："+SJLT_CHANNLID_QTWJ,"log"+SITEID,0);
		Util.log("当前发布文章所属栏目："+CHANNELID,"log"+SITEID,0);
		System.out.println("需要推送栏目规章："+SJLT_CHANNLID_GZ);
		System.out.println("需要推送栏目行政规范性文件："+SJLT_CHANNLID_XZGFXWJ);
		System.out.println("需要推送栏目其他文件文件："+SJLT_CHANNLID_QTWJ);
		String [] cidgz= SJLT_CHANNLID_GZ.split(";");
		String [] cidgfxwj= SJLT_CHANNLID_XZGFXWJ.split(";");
		String [] cidqtwj= SJLT_CHANNLID_QTWJ.split(";");
		String GFWJ_flag="GZ";
		//处理规则
		for (int i = 0; i < cidgz.length; i++) {
			String cnid=cidgz[i];
			if(CHANNELID.equals(cnid)){
				System.out.println("==========================================================处理规章类稿件开始==========================================================");
				pushDoc.pushDocument(jsondata,stauts,GFWJ_flag);
				System.out.println("==========================================================处理规章类稿件结束==========================================================");

			}
		}
		//处理规范性文件
		for (int i = 0; i < cidgfxwj.length; i++) {
			String cnid=cidgfxwj[i];
			if(CHANNELID.equals(cnid)){
				GFWJ_flag="GFXWJ";
				System.out.println("==========================================================处理行政规范性类稿件开始==========================================================");
				pushDoc.pushDocument(jsondata,stauts,GFWJ_flag);
				System.out.println("==========================================================处理行政规范性类稿件开始==========================================================");

			}
		}

		//处理其他文件文件
		for (int i = 0; i < cidqtwj.length; i++) {
			String cnid=cidqtwj[i];
			if(CHANNELID.equals(cnid)){
				GFWJ_flag="QTWJ";
				System.out.println("==========================================================处理行政规范性类稿件开始==========================================================");
				pushDoc.pushDocument(jsondata,stauts,GFWJ_flag);
				System.out.println("==========================================================处理行政规范性类稿件开始==========================================================");

			}
		}

		System.out.println("-----------------------------========================================================MQ推送文章结束="+stauts+"=========================================================----------------------------------");


	}
}
