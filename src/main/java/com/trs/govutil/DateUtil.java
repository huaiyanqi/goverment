package com.trs.govutil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {


    /**
     * Date转换为指定格式字符转
     *
     * @param date
     * @param formatParame
     * @return
     */
    public static String formateDay(Date date, String formatParame) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatParame);
        String result = sdf.format(date);
        return result;
    }


    /**
     * String转date类型
     * @param dateString
     * @param formatParame
     * @return
     * @throws ParseException
     */
    public static Date formateDay(String  dateString, String formatParame) throws ParseException {
        Date date = new SimpleDateFormat(formatParame).parse(dateString);
        return date;
    }


    /**
     * 获得当前时间
     *
     * @param formatParame
     * @return
     */
    public static String nowDay(String formatParame) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatParame);
        String result = sdf.format(new Date());
        return result;
    }

    /**
     * 获得今天一年以前的时间(精确到天)
     *
     * @return
     */
    public static String oneYearAgo(String formatParame) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatParame);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, -1);
        Date y = c.getTime();
        String result = sdf.format(y);
        return result;
    }


    /**
     * 获得今天一周以前的时间(精确到天)
     *
     * @return
     */
    public static String oneWeekAgo(String formatParame) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatParame);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -7);
        Date y = c.getTime();
        String result = sdf.format(y);
        return result;
    }


    /**
     * 获得今天一月以前的时间(精确到天)
     *
     * @return
     */
    public static String oneMonthAgo(String formatParame) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatParame);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        Date y = c.getTime();
        String result = sdf.format(y);
        return result;
    }


    /**
     * 获得今天一天以前的时间(精确到天)
     *
     * @return 字符串类型数据
     */
    public static String oneDayAgo(String formatParame) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatParame);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR_OF_DAY, -24);
        Date y = c.getTime();
        String result = sdf.format(y);
        return result;
    }


    /**
     * 获得当前一小时以前的时间
     *
     * @return 字符串类型数据
     */
    public static String oneHourAgo(String formatParame) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatParame);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR_OF_DAY, -1);
        Date y = c.getTime();
        String result = sdf.format(y);
        return result;
    }


    /**
     * 获得今天一天以前的时间(精确到天)
     *
     * @return Date类型数据
     */
    public static Date oneDayAgo() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR_OF_DAY, -24);
        Date y = c.getTime();
        return y;
    }


    /**
     * 获得今天一月以前的时间(精确到天)
     *
     * @return Date类型数据
     */
    public static Date oneMonthAgo() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        Date y = c.getTime();
        return y;
    }


    /**
     * 转为XXXX年XX月XX日
     * @param date
     * @return
     */
//    public static String toDayString(Date date) {
//        Calendar c = Calendar.getInstance();
//        c.setTime(date);
//        int year = c.get(Calendar.YEAR);
//        int month = c.get(Calendar.MONTH) + 1;
//        int day = c.get(Calendar.DAY_OF_MONTH);
//        return year + "年" + month + "月" + day + "日";
//    }


    public static String toDayString(Date date) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time = simpleDateFormat.format(date);
//        System.out.println(time);
        StringBuffer dt=new StringBuffer(time);
        dt.replace(4,5,"年");
        dt.replace(7,8,"月");
//        System.out.println(dt+"日");
        return dt+"日";
    }

    public static void main(String[] args) {


    }

}
