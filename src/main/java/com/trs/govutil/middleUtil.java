package com.trs.govutil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.trs.hy.HyUtil;
import com.trs.jdbc.JDBC;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class middleUtil {
	
//	private static String Z_URL = ConstantUtil.Z_URL;
//	private static String ZWBJ_APPID = ConstantUtil.ZWBJ_APPID;
//	private static String ZWBJ_STR_DEFAULT_KEY = ConstantUtil.ZWBJ_STR_DEFAULT_KEY;
//	private static String USER = ConstantUtil.USER;
    
	/**
	 * 查询栏目是否存在下级栏目 没有删除  有继续查询删除
	 * pid  为要查询的栏目，下级是否存在栏目 
	 * @throws Exception
	 */
	public static void selectLM(String sitId,String pid,String userName) throws Exception{
		
		// 根据站点id查询下级栏目  查询当前栏目的下级栏目
		String sServiceId = "gov_site";
		String sMethodName = "queryChildrenChannelsOnEditorCenter";
		Map<String, String> mSE=new HashMap<String, String>();
		mSE.put("CurrUserName", userName); // 当前操作的用户
		//设置一次查询数量
		mSE.put("pageSize", "15000");
		mSE.put("pageindex", "1");
		mSE.put("SITEID", sitId);
		mSE.put("ParentChannelId", pid);
		String hy1=HyUtil.dataMoveDocumentHyRbj(sServiceId,sMethodName,mSE);
		System.out.println("结果"+hy1);
		JSONObject jsonObject1 = JSON.parseObject(hy1);
		Object dataArr1 = jsonObject1.get("DATA");
		if("true".equals(jsonObject1.get("ISSUCCESS"))){
			JSONObject jsonObjectHYName = JSON.parseObject(dataArr1.toString());
			JSONArray hy2=jsonObjectHYName.getJSONArray("DATA");
			if(hy2.size()>0){									
				for(int a=0;a<hy2.size();a++){
					JSONObject dataBean = (JSONObject) hy2.get(a);
					String CHNLDESC=dataBean.getString("CHNLDESC");//栏目名称
					String CHANNELID=dataBean.getString("CHANNELID");//栏目id
					String HASCHILDREN=dataBean.getString("HASCHILDREN");//是否存在下级栏目
					System.out.println(sitId+"-------------------"+CHNLDESC+"----------------"+CHANNELID);
					
					if("true".equals(HASCHILDREN)){
						//代表存在下级栏目   先删除栏目，以及栏目下数据
						//删除栏目
						deleteLm(CHANNELID,sitId,CHNLDESC);
						//删除数据
						deleteDocument(CHANNELID,sitId,CHNLDESC);
						//继续查询下级栏目
						selectLM(sitId,CHANNELID,userName);
					}else{
						//无下级栏目 ，直接删除该栏目，以及栏目下的数据
						//删除栏目
						deleteLm(CHANNELID,sitId,CHNLDESC);
						//删除数据
						deleteDocument(CHANNELID,sitId,CHNLDESC);
					}
					
				}
			}else{
				System.out.println("该栏目下无下级栏目！！！！！！！！！！");
			}
		}
	}
    
	
	/**
	 * 删除中间表栏目数据
	 * @throws Exception
	 */
	public static void deleteLm(String CHANNELID,String siteid,String channelname) throws Exception{
		
		JDBC jdbc=new JDBC();
		Integer delete=0;
		try {
			Util.log("删除中间表栏目关系数据sql：","delLMlog",0);
			Util.log("DELETE  FROM hy_middleapp where source='LM' and haiyun_id='"+CHANNELID+"' and siteid='"+siteid+"'","delleteLMlog"+siteid,0);
			System.out.println("删除中间表栏目关系数据sql：");
			System.out.println("DELETE  FROM hy_middleapp where source='LM' and haiyun_id='"+CHANNELID+"' and siteid='"+siteid+"'");			
			delete = jdbc.JDBCDriver("DELETE  FROM hy_middleapp where source='LM' and haiyun_id='"+CHANNELID+"' and siteid='"+siteid+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(delete==1){
			Util.log(CHANNELID+"-----------------"+siteid+"-----------------"+channelname,"delleteLMlog"+siteid,0);
			Util.log("中间表删除成功!","delleteLMlog"+siteid,0);
			System.out.println("中间表删除成功!");
		}else{
			Util.log(CHANNELID+"-----------------"+siteid+"-----------------"+channelname,"delleteLMlog"+siteid,0);
			Util.log("中间表删除失败!","delleteLMlog"+siteid,0);
			System.out.println("中间表删除失败!");
		}
	}
	
	
	/**
	 * 删除中间表文章数据
	 * @throws Exception
	 */
	public static void deleteDocument(String CHANNELID,String siteid,String channelname) throws Exception{
		
		JDBC jdbc=new JDBC();
		Integer delete=0;
		try {
			Util.log("删除中间表栏目下数据sql：","delleteLMlog"+siteid,0);
			Util.log("DELETE  FROM document where source='WD' and chnlid='"+CHANNELID+"' and siteid='"+siteid+"'","delleteLMlog"+siteid,0);
			System.out.println("删除中间表栏目下数据sql：");
			System.out.println("DELETE  FROM document where source='WD' and chnlid='"+CHANNELID+"' and siteid='"+siteid+"'");
			delete = jdbc.JDBCDriver("DELETE  FROM document where source='WD' and chnlid='"+CHANNELID+"' and siteid='"+siteid+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(delete==1){
			Util.log(CHANNELID+"-----------------"+siteid+"-----------------"+channelname,"delleteLMlog"+siteid,0);
			Util.log("中间表删除成功!","delDocumentlog",0);
			System.out.println("中间表删除成功!");
		}else{
			Util.log(CHANNELID+"-----------------"+siteid+"-----------------"+channelname,"delleteLMlog"+siteid,0);
			Util.log("中间表删除失败!","delDocumentlog",0);
			System.out.println("中间表删除失败!");
		}
	}
}
