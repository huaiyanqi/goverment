package com.trs.govutil;

public enum StatusEnum {

    add("新增"), update("更新"), delete("删除"), abandon("废止");

    private String cnName;

    StatusEnum(String cnName) {
        this.cnName = cnName;
    }
}
