package com.trs.govutil;

import java.util.ResourceBundle;


import java.util.ResourceBundle;

public class ConstantUtil {

	static ResourceBundle resource = ResourceBundle.getBundle("services");



	public static final String LOCALLOG = resource.getString("LOCALLOG");


	//MQ
	public static final String MQ_ADDRESSES = resource.getString("MQ_ADDRESSES");

	public static final String MQ_PORT = resource.getString("MQ_PORT");

	public static final String MQ_USERNAME = resource.getString("MQ_USERNAME");

	public static final String MQ_PASSWORD = resource.getString("MQ_PASSWORD");

	public static final String MQ_VIRTUALHOST = resource.getString("MQ_VIRTUALHOST");





	//jdbc
	public static final String jdbc_driver = resource.getString("jdbc_driver");

	public static final String jdbc_url = resource.getString("jdbc_url");

	public static final String jdbc_username = resource.getString("jdbc_username");

	public static final String jdbc_password = resource.getString("jdbc_password");

	public static final String TABLE = resource.getString("TABLE");

	public static final String FIELDKP = resource.getString("FIELDKP");

	public static final String FIELDHY = resource.getString("FIELDHY");

	public static final String SOURCE = resource.getString("SOURCE");

	public static final String PARENTID = resource.getString("PARENTID");


	public static final String jdbc_urlids =resource.getString("jdbc_urlIIP");

	public static final String jdbc_usernameids = resource.getString("jdbc_usernameIIP");

	public static final String jdbc_passwordids = resource.getString("jdbc_passwordIIP");



	public static final String jdbc_urlIIP =resource.getString("jdbc_urlIIP");

	public static final String jdbc_usernameIIP = resource.getString("jdbc_usernameIIP");

	public static final String jdbc_passwordIIP = resource.getString("jdbc_passwordIIP");



	//操作用户
	public static final String USER = resource.getString("USER");

	//连接wcm数据库
	public static final String jdbc_urlwcm =resource.getString("jdbc_urlwcm");
	public static final String jdbc_usernamewcm = resource.getString("jdbc_usernamewcm");
	public static final String jdbc_passwordwcm = resource.getString("jdbc_passwordwcm");


	public static final String SJLT_CHANNLID_GZ =resource.getString("SJLT_CHANNLID_GZ");
	public static final String SJLT_CHANNLID_XZGFXWJ =resource.getString("SJLT_CHANNLID_XZGFXWJ");
	public static final String SJLT_CHANNLID_QTWJ =resource.getString("SJLT_CHANNLID_QTWJ");
	public static final String SITE_POSTDEPT =resource.getString("SITE_POSTDEPT");
	public static final String SJLT_URL =resource.getString("SJLT_URL");
	public static final String HY_DOW_URL =resource.getString("HY_DOW_URL");

	public static final String Content_regular = resource.getString("Content_regular");

	public static final String Content_regular_p = resource.getString("Content_regular_p");


}

